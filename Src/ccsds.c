/*
 *  AX5043 OS-independent driver
 *
 *  Copyright (C) 2019 Libre Space Foundation (https://libre.space)
 *
 *  Authors: surligas, ctriant
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifdef MSB_FIRST
#define XOR(A,k,b)     (A[k/8] & ~(1 << (7-k%8))) | \
                            ((((A[k/8] >> (7-k%8)) & 1) ^ b) << (7-k%8))
#else
#define XOR(A,k,b)     (A[k/8] & ~(1 << k%8)) | \
                            ((((A[k/8] >> k%8) & 1) ^ b) << k%8)
#endif

#include "ccsds.h"

/**
 * Scrambles the data in place, based on the CCSDS Randomization procedure
 * Initial seed as specified by CCSDS 231.0-B-3 is all ones
 * @param data data to randomize. The result is available in place
 * @param len the number of bytes to randomize
 */
void
ccsds_scrambler(uint8_t *data, size_t len)
{
	int cnt = 0;
	while (cnt < len * 8) {
		data[cnt / 8] = XOR(data, cnt,
		                    ((CODEWORD[(cnt % CODEWORD_SIZE_BITS) / 8] >> (cnt % 8)) & 1));
		cnt++;
	}

}

/**
 * Descrambles the data in place, based on the CCSDS Randomization procedure
 * Initial seed as specified by CCSDS 231.0-B-3 is all ones
 * @param data data to de-randomize. The result is available in place
 * @param len the number of bytes to de-randomize
 *
 * @note: While this is an additive synchronous scrambler, scrambling and
 * descrambling is the exact same procedure. This function is only for
 * readability purposes.
 */
void
ccsds_descrambler(uint8_t *data, size_t len)
{
	int cnt = 0;
	while (cnt < len * 8) {
		data[cnt / 8] = XOR(data, cnt,
		                    ((CODEWORD[(cnt % CODEWORD_SIZE_BITS) / 8] >> (cnt % 8)) & 1));
		cnt++;
	}
}
