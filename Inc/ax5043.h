/*
 *  AX5043 OS-independent driver
 *
 *  Copyright (C) 2019, 2020 Libre Space Foundation (https://libre.space)
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef AX5043_H_
#define AX5043_H_

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

/**
 * Utility functions
 */
#ifndef BIT
#define BIT(x)   (1 << (x))
#endif

#ifndef max
#define max(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b;       \
})
#endif

#ifndef min
#define min(a,b)             \
({                           \
    __typeof__ (a) _a = (a); \
    __typeof__ (b) _b = (b); \
    _a < _b ? _a : _b;       \
})
#endif

/**
 * The maximum allowed frame size
 */
#define MAX_FRAME_LEN                   1024

/**
 * The maximum allowed SPI transfer size
 */
#define SPI_MAX_TRASNFER_SIZE           512

#define MIN_RF_FREQ_INT_VCO_RFDIV0      800000000
#define MAX_RF_FREQ_INT_VCO_RFDIV0      1050000000

#define MIN_RF_FREQ_INT_VCO_RFDIV1      (MIN_RF_FREQ_INT_VCO_RFDIV0 / 2)
#define MAX_RF_FREQ_INT_VCO_RFDIV1      (MAX_RF_FREQ_INT_VCO_RFDIV0 / 2)

#define MIN_RF_FREQ_EXT_VCO_RFDIV0      54000000
#define MAX_RF_FREQ_EXT_VCO_RFDIV0      525000000

#define MIN_RF_FREQ_EXT_VCO_RFDIV1      (MIN_RF_FREQ_EXT_VCO_RFDIV0 / 2)
#define MAX_RF_FREQ_EXT_VCO_RFDIV1      (MAX_RF_FREQ_EXT_VCO_RFDIV0 / 2)

#define MIN_FXTAL                       10e6
#define MAX_FXTAL                       50e6

#define AX5043_OUTPUT_POWER             0x000

/******************************************************************************
 ********************  AX5043 control SPI registers ***************************
 *****************************************************************************/

/* Status and test registers */
#define AX5043_REG_REV                  0x0
#define AX5043_REG_SCRATCH              0x1

/* Power and voltage regulator */
#define AX5043_REG_PWRMODE              0x2
#define AX5043_REG_POWSTAT              0x3
#define AX5043_REG_POWSTICKYSTAT        0x4
#define AX5043_REG_POWIRQMASK           0x5

/* Interrupt control */
#define AX5043_REG_IRQMASK1             0x6
#define AX5043_REG_IRQMASK0             0x7
#define AX5043_REG_RADIOEVENTMASK1      0x8
#define AX5043_REG_RADIOEVENTMASK0      0x9

#define AX5043_REG_IRQREQUEST1          0xC
#define AX5043_REG_IRQREQUEST0          0xD
#define AX5043_REG_RADIOEVENTREQ1       0xE
#define AX5043_REG_RADIOEVENTREQ0       0xF

/* Modulation and framing */
#define AX5043_REG_MODULATION           0x010
#define AX5043_REG_ENCODING             0x011
#define AX5043_REG_FRAMING              0x012
#define AX5043_REG_CRCINIT3             0x014
#define AX5043_REG_CRCINIT2             0x015
#define AX5043_REG_CRCINIT1             0x016
#define AX5043_REG_CRCINIT0             0x017

/* FEC */
#define AX5043_REG_FEC                  0x018
#define AX5043_REG_FECSYNC              0x019
#define AX5043_REG_FECSTATUS            0x01A

/* Status */
#define AX5043_REG_RADIOSTATE           0x01C
#define AX5043_REG_XTALSTATUS           0x01D

/* Pin configuration */
#define AX5043_REG_PINSTATE             0x20
#define AX5043_REG_PINFUNCSYSCLK        0x21
#define AX5043_REG_PINFUNCDCLK          0x22
#define AX5043_REG_PINFUNCDATA          0x23
#define AX5043_REG_PINFUNCIRQ           0x24
#define AX5043_REG_PINFUNCANTSEL        0x25
#define AX5043_REG_PINFUNCPWRAMP        0x26
#define AX5043_REG_PWRAMP               0x27

/* FIFO control */
#define AX5043_REG_FIFOSTAT             0x28
#define AX5043_REG_FIFODATA             0x29
#define AX5043_REG_FIFOCOUNT1           0x2A
#define AX5043_REG_FIFOCOUNT0           0x2B
#define AX5043_REG_FIFOFREE1            0x2C
#define AX5043_REG_FIFOFREE0            0x2D
#define AX5043_REG_FIFOTHRESH1          0x2E
#define AX5043_REG_FIFOTHRESH0          0x2F

/* Frequency Synthesizer */
#define AX5043_REG_PLLLOOP              0x30
#define AX5043_REG_PLLCPI               0x31
#define AX5043_REG_PLLVCODIV            0x32
#define AX5043_REG_PLLRANGINGA          0x33
#define AX5043_REG_FREQA3               0x34
#define AX5043_REG_FREQA2               0x35
#define AX5043_REG_FREQA1               0x36
#define AX5043_REG_FREQA0               0x37
#define AX5043_REG_PLLLOOPBOOST         0x38
#define AX5043_REG_PLLCPIBOOST          0x39
#define AX5043_REG_PLLRANGINGB          0x3B
#define AX5043_REG_FREQB3               0x3C
#define AX5043_REG_FREQB2               0x3D
#define AX5043_REG_FREQB1               0x3E
#define AX5043_REG_FREQB0               0x3F

/* RSSI */
#define AX5043_REG_RSSI                 0x40
#define AX5043_REG_BGNDRSSI             0x41
#define AX5043_REG_DIVERSITY            0x42
#define AX5043_REG_AGCCOUNTER           0x43

/* Receiver Tracking */
#define AX5043_REG_TRKDATARATE2         0x45
#define AX5043_REG_TRKDATARATE1         0x46
#define AX5043_REG_TRKDATARATE0         0x47
#define AX5043_REG_TRKAMPL1             0x48
#define AX5043_REG_TRKAMPL0             0x49
#define AX5043_REG_TRKPHASE1            0x4A
#define AX5043_REG_TRKPHASE0            0x4B
#define AX5043_REG_TRKRFFREQ2           0x4D
#define AX5043_REG_TRKRFFREQ1           0x4E
#define AX5043_REG_TRKRFFREQ0           0x4F
#define AX5043_REG_TRKFREQ1             0x50
#define AX5043_REG_TRKFREQ0             0x51
#define AX5043_REG_TRKFSKDEMOD1         0x52
#define AX5043_REG_TRKFSKDEMOD0         0x53

/* Timers */
#define AX5043_REG_TIMER2               0x59
#define AX5043_REG_TIMER1               0x5A
#define AX5043_REG_TIMER0               0x5B

/* Wakeup timer */
#define AX5043_REG_WAKEUPTIMER1         0x68
#define AX5043_REG_WAKEUPTIMER0         0x69
#define AX5043_REG_WAKEUP1              0x6A
#define AX5043_REG_WAKEUP0              0x6B
#define AX5043_REG_WAKEUPFREQ1          0x6C
#define AX5043_REG_WAKEUPFREQ0          0x6D
#define AX5043_REG_WAKEUPXOEARLY        0x6E

/* PHY related registers*/
#define AX5043_REG_IFFREQ1              0x100
#define AX5043_REG_IFFREQ0              0x101
#define AX5043_REG_DECIMATION           0x102
#define AX5043_REG_RXDATARATE2          0x103
#define AX5043_REG_RXDATARATE1          0x104
#define AX5043_REG_RXDATARATE0          0x105
#define AX5043_REG_MAXDROFFSET2         0x106
#define AX5043_REG_MAXDROFFSET1         0x107
#define AX5043_REG_MAXDROFFSET0         0x108
#define AX5043_REG_MAXRFOFFSET2         0x109
#define AX5043_REG_MAXRFOFFSET1         0x10A
#define AX5043_REG_MAXRFOFFSET0         0x10B
#define AX5043_REG_FSKDMAX1             0x10C
#define AX5043_REG_FSKDMAX0             0x10D
#define AX5043_REG_FSKDMIN1             0x10E
#define AX5043_REG_FSKDMIN0             0x10F
#define AX5043_REG_AFSKSPACE1           0x110
#define AX5043_REG_AFSKSPACE0           0x111
#define AX5043_REG_AFSKMARK1            0x112
#define AX5043_REG_AFSKMARK0            0x113
#define AX5043_REG_AFSKCTRL             0x114
#define AX5043_REG_AMPLFILTER           0x115
#define AX5043_REG_FREQUENCYLEAK        0x116
#define AX5043_REG_RXPARAMSETS          0x117
#define AX5043_REG_RXPARAMCURSET        0x118

/* Receiver Parameter Set 0 */
#define AX5043_REG_AGCGAIN0             0x120
#define AX5043_REG_AGCTARGET0           0x121
#define AX5043_REG_AGCAHYST0            0x122
#define AX5043_REG_AGCMINMAX0           0x123
#define AX5043_REG_TIMEGAIN0            0x124
#define AX5043_REG_DRGAIN0              0x125
#define AX5043_REG_PHASEGAIN0           0x126
#define AX5043_REG_FREQGAINA0           0x127
#define AX5043_REG_FREQGAINB0           0x128
#define AX5043_REG_FREQGAINC0           0x129
#define AX5043_REG_FREQGAIND0           0x12A
#define AX5043_REG_AMPLGAIN0            0x12B
#define AX5043_REG_FREQDEV10            0x12C
#define AX5043_REG_FREQDEV00            0x12D
#define AX5043_REG_FOURFSK0             0x12E
#define AX5043_REG_BBOFFSRES0           0x12F

/* Receiver Parameter Set 1 */
#define AX5043_REG_AGCGAIN1             0x130
#define AX5043_REG_AGCTARGET1           0x131
#define AX5043_REG_AGCAHYST1            0x132
#define AX5043_REG_AGCMINMAX1           0x133
#define AX5043_REG_TIMEGAIN1            0x134
#define AX5043_REG_DRGAIN1              0x135
#define AX5043_REG_PHASEGAIN1           0x136
#define AX5043_REG_FREQGAINA1           0x137
#define AX5043_REG_FREQGAINB1           0x138
#define AX5043_REG_FREQGAINC1           0x139
#define AX5043_REG_FREQGAIND1           0x13A
#define AX5043_REG_AMPLGAIN1            0x13B
#define AX5043_REG_FREQDEV11            0x13C
#define AX5043_REG_FREQDEV01            0x13D
#define AX5043_REG_FOURFSK1             0x13E
#define AX5043_REG_BBOFFSRES1           0x13F

/* Receiver Parameter Set 2 */
#define AX5043_REG_AGCGAIN2             0x140
#define AX5043_REG_AGCTARGET2           0x141
#define AX5043_REG_AGCAHYST2            0x142
#define AX5043_REG_AGCMINMAX2           0x143
#define AX5043_REG_TIMEGAIN2            0x144
#define AX5043_REG_DRGAIN2              0x145
#define AX5043_REG_PHASEGAIN2           0x146
#define AX5043_REG_FREQGAINA2           0x147
#define AX5043_REG_FREQGAINB2           0x148
#define AX5043_REG_FREQGAINC2           0x149
#define AX5043_REG_FREQGAIND2           0x14A
#define AX5043_REG_AMPLGAIN2            0x14B
#define AX5043_REG_FREQDEV12            0x14C
#define AX5043_REG_FREQDEV02            0x14D
#define AX5043_REG_FOURFSK2             0x14E
#define AX5043_REG_BBOFFSRES2           0x14F

/* Receiver Parameter Set 3 */
#define AX5043_REG_AGCGAIN3             0x150
#define AX5043_REG_AGCTARGET3           0x151
#define AX5043_REG_AGCAHYST3            0x152
#define AX5043_REG_AGCMINMAX3           0x153
#define AX5043_REG_TIMEGAIN3            0x154
#define AX5043_REG_DRGAIN3              0x155
#define AX5043_REG_PHASEGAIN3           0x156
#define AX5043_REG_FREQGAINA3           0x157
#define AX5043_REG_FREQGAINB3           0x158
#define AX5043_REG_FREQGAINC3           0x159
#define AX5043_REG_FREQGAIND3           0x15A
#define AX5043_REG_AMPLGAIN3            0x15B
#define AX5043_REG_FREQDEV13            0x15C
#define AX5043_REG_FREQDEV03            0x15D
#define AX5043_REG_FOURFSK3             0x15E
#define AX5043_REG_BBOFFSRES3           0x15F

/* Transmitter Parameters */
#define AX5043_REG_MODCFGF              0x160
#define AX5043_REG_FSKDEV2              0x161
#define AX5043_REG_FSKDEV1              0x162
#define AX5043_REG_FSKDEV0              0x163
#define AX5043_REG_MODCFGA              0x164
#define AX5043_REG_TXRATE2              0x165
#define AX5043_REG_TXRATE1              0x166
#define AX5043_REG_TXRATE0              0x167
#define AX5043_REG_TXPWRCOEFFA1         0x168
#define AX5043_REG_TXPWRCOEFFA0         0x169
#define AX5043_REG_TXPWRCOEFFB1         0x16A
#define AX5043_REG_TXPWRCOEFFB0         0x16B
#define AX5043_REG_TXPWRCOEFFC1         0x16C
#define AX5043_REG_TXPWRCOEFFC0         0x16D
#define AX5043_REG_TXPWRCOEFFD1         0x16E
#define AX5043_REG_TXPWRCOEFFD0         0x16F
#define AX5043_REG_TXPWRCOEFFE1         0x170
#define AX5043_REG_TXPWRCOEFFE0         0x171

/* PLL parameters */
#define AX5043_REG_PLLVCOI              0x180
#define AX5043_REG_PLLVCOIR             0x181
#define AX5043_REG_PLLLOCKDET           0x182
#define AX5043_REG_PLLRNGCLK            0x183

/* Crystal Oscillator */
#define AX5043_REG_XTALCAP              0x184

/* Baseband */
#define AX5043_REG_BBTUNE               0x188
#define AX5043_REG_BBOFFSCAP            0x189

/* MAC parameters */

/* Packet Format */
#define AX5043_REG_PKTADDRCFG           0x200
#define AX5043_REG_PKTLENCFG            0x201
#define AX5043_REG_PKTLENOFFSET         0x202
#define AX5043_REG_PKTMAXLEN            0x203
#define AX5043_REG_PKTADDR3             0x204
#define AX5043_REG_PKTADDR2             0x205
#define AX5043_REG_PKTADDR1             0x206
#define AX5043_REG_PKTADDR0             0x207
#define AX5043_REG_PKTADDRMASK3         0x208
#define AX5043_REG_PKTADDRMASK2         0x209
#define AX5043_REG_PKTADDRMASK1         0x20A
#define AX5043_REG_PKTADDRMASK0         0x20B

/* Pattern Match */
#define AX5043_REG_MATCH0PAT3           0x210
#define AX5043_REG_MATCH0PAT2           0x211
#define AX5043_REG_MATCH0PAT1           0x212
#define AX5043_REG_MATCH0PAT0           0x213
#define AX5043_REG_MATCH0LEN            0x214
#define AX5043_REG_MATCH0MIN            0x215
#define AX5043_REG_MATCH0MAX            0x216
#define AX5043_REG_MATCH1PAT1           0x218
#define AX5043_REG_MATCH1PAT0           0x219
#define AX5043_REG_MATCH1LEN            0x21C
#define AX5043_REG_MATCH1MIN            0x21D
#define AX5043_REG_MATCH1MAX            0x21E

/* Packet Controller */
#define AX5043_REG_TMGTXBOOST           0x220
#define AX5043_REG_TMGTXSETTLE          0x221
#define AX5043_REG_TMGRXBOOST           0x223
#define AX5043_REG_TMGRXSETTLE          0x224
#define AX5043_REG_TMGRXOFFSACQ         0x225
#define AX5043_REG_TMGRXCOARSEAGC       0x226
#define AX5043_REG_TMGRXAGC             0x227
#define AX5043_REG_TMGRXRSSI            0x228
#define AX5043_REG_TMGRXPREAMBLE1       0x229
#define AX5043_REG_TMGRXPREAMBLE2       0x22A
#define AX5043_REG_TMGRXPREAMBLE3       0x22B
#define AX5043_REG_RSSIREFERENCE        0x22C
#define AX5043_REG_RSSIABSTHR           0x22D
#define AX5043_REG_BGNDRSSIGAIN         0x22E
#define AX5043_REG_BGNDRSSITHR          0x22F
#define AX5043_REG_PKTCHUNKSIZE         0x230
#define AX5043_REG_PKTMISCFLAGS         0x231
#define AX5043_REG_PKTSTOREFLAGS        0x232
#define AX5043_REG_PKTACCEPTFLAGS       0x233

/* Special Functions */

/* General Purpose ADC */
#define AX5043_REG_GPADCCTRL            0x300
#define AX5043_REG_GPADCPERIOD          0x301
#define AX5043_REG_GPADC13VALUE1        0x308
#define AX5043_REG_GPADC13VALUE0        0x309

/* Low Power Oscillator Calibration */
#define AX5043_REG_LPOSCCONFIG          0x310
#define AX5043_REG_LPOSCSTATUS          0x311
#define AX5043_REG_LPOSCKFILT1          0x312
#define AX5043_REG_LPOSCKFILT0          0x313
#define AX5043_REG_LPOSCREF1            0x314
#define AX5043_REG_LPOSCREF0            0x315
#define AX5043_REG_LPOSCFREQ1           0x316
#define AX5043_REG_LPOSCFREQ0           0x317
#define AX5043_REG_LPOSCPER1            0x318
#define AX5043_REG_LPOSCPER0            0x319

/* Performance Tuning Registers */
#define AX5043_REG_XTALDIV              0xF35

/******************************************************************************
 ************************* Register values ************************************
 *****************************************************************************/
#define AX5043_REV                      0x51
#define AX5043_SCRATCH_TEST             0xAA

#define AX5043_RADIO_STATE_IDLE         0x0

#define AX5043_PLLVCOI_MANUAL           BIT(7)

/**
 * Modem Domain Voltage Regulator Ready
 */
#define AX5043_SVMODEM                  BIT(3)

/**
 * Init value for the VCO prior starting an autoranging
 */
#define AX5043_VCOR_INIT                8

#define AX5043_RFDIV0                   0x0
#define AX5043_RFDIV1                   BIT(2)

#define AX5043_FREQSHAPE_EXT_FILTER     0x0
#define AX5043_FREQSHAPE_INVALID        0x1
#define AX5043_FREQSHAPE_GAUSSIAN_BT_03 0x2
#define AX5043_FREQSHAPE_GAUSSIAN_BT_05 0x3

/**
 *  Non-coherent ASK modulation mode
 */
#define AX5043_MODULATION_ASK           BIT(0)

/**
 *  FSK modulation mode
 */
#define AX5043_MODULATION_FSK           BIT(3)

#define AX5043_ENC_INV                  BIT(0)
#define AX5043_ENC_DIFF                 BIT(1)
#define AX5043_ENC_SCRAM                BIT(2)

/**
 * HDLC Framing mode
 */
#define AX5043_HDLC_FRAMING             BIT(2)

/**
 * HDLC compliant CRC16
 */
#define AX5043_CRC16_CCITT              BIT(4)

/**
 * Set the FIFO to variable length data mode
 */
#define AX5043_FIFO_VARIABLE_DATA_CMD   0xe1

#define AX5043_FIFO_REPEATDATA_CMD      (BIT(6) | BIT(5) | BIT(1))
#define AX5043_FIFO_TXCTRL_CMD          (BIT(5) | BIT(4) | BIT(3) | BIT(2))

/**
 * Poweramp pin function
 */
#define AX5043_POWERAMP_MODE_OUTPUT     (BIT(2) | BIT(1))
#define AX5043_POWERAMP_MODE_PULLUP     (BIT(7) | BIT(1))
#define AX5043_POWERAMP_MODE_DAC        (BIT(2) | BIT(0))
/**
 * FIFO commit command
 */
#define AX5043_FIFO_COMMIT_CMD          BIT(2)
#define AX5043_FIFO_PKTSTART            BIT(0)
#define AX5043_FIFO_PKTEND              BIT(1)
#define AX5043_FIFO_NOCRC               BIT(3)
#define AX5043_FIFO_RAW                 BIT(4)

/**
 * Maximum chuck that can be committed on the FIFO
 */
#define AX5043_PKTCHUNKSIZE_240         0xd

#define AX5043_FIFO_MAX_SIZE            240

/**
 * When this threshold of free bytes in the TX FIFO is reached,
 * an IRQ is raised
 */
#define AX5043_FIFO_FREE_THR            128

#define AX5043_IRQMFIFOTHRFREE          BIT(3)
#define AX5043_IRQRFIFOTHRFREE          BIT(3)
#define AX5043_IRQMRADIOCTRL            BIT(6)


/*
 * RADIOEVENTMASK1, RADIOEVENTMASK0 Radio event IRQ mask values
 */
#define AX5043_REVMDONE                 BIT(0)
#define AX5043_REVMSETTLED              BIT(1)
#define AX5043_REVMRADIOSTATECHG        BIT(2)
#define AX5043_REVMRXPARAMSETCHG        BIT(3)
#define AX5043_REVMFRAMECLK             BIT(4)

/*
 * RADIOEVENTREQ1, RADIOEVENTREQ0
 */
#define AX5043_REVRDONE                 BIT(0)
#define AX5043_REVRSETTLED              BIT(1)
#define AX5043_REVRRADIOSTATECHG        BIT(2)
#define AX5043_REVRRXPARAMSETCHG        BIT(3)
#define AX5043_REVRFRAMECLK             BIT(4)



/**
 * Frequency mode A or B actually selects at which registers
 * the frequency configuration should be written.
 *
 * This is quite handy for different RX/TX frequencies, to avoid
 * writing every time the two different frequency configurations.
 */
typedef enum {
	FREQA_MODE = 0,               //!< FREQA_MODE
	FREQB_MODE = 1                //!< FREQB_MODE
} freq_mode_t;

typedef enum {
	VCO_INTERNAL = 0,
	VCO_EXTERNAL = 1
} vco_mode_t;

typedef enum {
	XTAL,
	TCXO,
	XTAL_INTERNAL
} xtal_t;

struct xtal {
	xtal_t type;
	float  freq;
	float  capacitance;
};

typedef enum {
	POWERDOWN = 0,
	DEEPSLEEP = 1,
	STANDBY = 0x5,
	FIFO_ENABLED = 0x6,
	SYNTHRX = 0x8,
	FULLRX = 0x9,
	WOR = 0xb,
	SYNTHTX = 0xc,
	FULLTX = 0xd
} power_mode_t;

typedef enum {
	ANTSEL_OUTPUT_0 = 0,
	ANTSEL_OUTPUT_1,
	ANTSEL_OUTPUT_Z,
	ANTSEL_OUTPUT_BB_TUBE_CLK,
	ANTSEL_OUTPUT_EXT_TCXO_EN,
	ANTSEL_OUTPUT_DAC,
	ANTSEL_OUTPUT_DIVERSITY,
	ANTSEL_OUTPUT_TEST_OBS
} pfantsel_t;

typedef enum {
	PWRAMP_OUTPUT_0 = 0x0,
	PWRAMP_OUTPUT_1 = 0x1,
	PWRAMP_OUTPUT_Z = 0x2,
	PWRAMP_INPUT_DIBIT_SYNC = 0x3,
	PWRAMP_OUTPUT_DIBIT_SYNC = 0x4,
	PWRAMP_OUTPUT_DAC = 0x5,
	PWRAMP_OUTPUT_PWRAMP = 0x6,
	PWRAMP_OUTPUT_TCXO_EN = 0x7,
	PWRAMP_OUTPUT_TEST_OBS = 0xF
} pfpwwramp_t;

/**
 * RF output mode, differential or single ended
 */
typedef enum {
	TXDIFF = 0,//!< Enable Differential Transmitter
	TXSE   //!< Enable Single ended Transmitter
} rf_out_t;

/**
 * TX Shaping
 */
typedef enum {
	UNSHAPED,//!< Unshaped
	RC       //!< Raised Cosine
} tx_shaping_t;

/**
 * TX Frequency Shaping
 */
typedef enum {
	EXTERNAL_FILTER = 0,//!< External loop filter
	INVALID,       		//!< Invalid
	GAUSIAN_BT_0_3, 	//!< Gausian 0.3
	GAUSIAN_BT_0_5  	//!< Gausian 0.5
} freq_shaping_t;

enum {
	AX5043_OK = 0,
	AX5043_INVALID_PARAM,
	AX5043_TIMEOUT,
	AX5043_NOT_FOUND,
	AX5043_AUTORANGING_ERROR,
	AX5043_NOT_READY,
	AX5043_NOT_IMPLEMENTED,
	AX5043_HDLC_INVALID_PARAM,
	AX5043_TX_INVALID,
	AX5043_RX_INVALID,
};

typedef enum {
	ASK = 0,
	ASK_COHERENT = BIT(0),
	PSK = BIT(2),
	OQPSK = BIT(2) | BIT(1),
	MSK = BIT(2) | BIT(1) | BIT(0),
	FSK = BIT(3),
	FSK_4 = BIT(3) | BIT(0),
	AFSK = BIT(3) | BIT(1),
	FM = BIT(3) | BIT(1) | BIT(0)
} modulation_t;

struct afsk_tx_params {
	uint32_t mark_freq;
	uint32_t space_freq;
};

struct fsk_tx_params {
	float           mod_index;
	uint8_t         order;
	freq_shaping_t  freq_shaping;
};

struct msk_tx_params {
	freq_shaping_t  freq_shaping;
};

struct psk_params {
	uint8_t order;
};

typedef enum {
	CRC_OFF = 0,
	CRC16_CCITT = 1,
	CRC16 = 2,
	CRC16_DNP = 3,
	CRC32 = 6
} crcmode_t;

/**
 * CRC mode
 */
struct crc {
	crcmode_t mode; /**< CRC mode */
	uint32_t init;  /**< Initial value for the CRC. LSB alligned for non 32-bit CRC */
};

struct encoding {
	uint8_t    inv;
	uint8_t    diff;
	uint8_t    scrambler;
	uint8_t    manch;
	uint8_t    nosync;
};

typedef enum {
	RAW = 0,
	RAW_SOFT,
	HDLC,
	RAW_PATTERN_MATCH,
	M_BUS,
	MBUS_4_6
} framing_t;

/**
 * Frequency offset correction implementation
 */
typedef enum {
	SECOND_LO = 0,             //!< Apply correction at the second LO (default)
	FIRST_LO                   //!< Apply correction at the first LO
} freq_correction_t;

typedef enum {
	LEAKAGE_0 = 0,
	LEAKAGE_1,
	LEAKAGE_2,
	LEAKAGE_5,
	LEAKAGE_11,
	LEAKAGE_22,
	LEAKAGE_44,
	LEAKAGE_88,
	LEAKAGE_177,
	LEAKAGE_355,
	LEAKAGE_709,
	LEAKAGE_1419,
	LEAKAGE_2839,
	LEAKAGE_5678,
	LEAKAGE_11356,
	LEAKAGE_22713
} leakage_t;

struct raw_framing {
	uint8_t    en_inv;
	uint8_t    en_diff;
	uint8_t    en_scrambler;
	uint8_t    en_manch;
	uint8_t    en_nosync;
	struct crc crc;
};

struct hdlc_framing {
	uint8_t en_nrz;
	uint8_t en_nrzi;
	uint8_t en_scrambler;
	/* CRC is specified by the HDLC protocol. No parameterization is allowed */
	/* Most of the times, HDLC transmissions repeat the preamble and postamble */
	uint32_t preamble_len;
	uint32_t postamble_len;
};

struct pattern_framing {
	uint16_t preamble;
	/**< Preamble should be a repeated pattern. The IC supports up  to 16 bits,
	     but of course the transmitter should transmit more. This will allow
	     the receiver to continuously switch on and off, reducing power
	     consumption. This preamble should be given in MSB first order. */
	size_t   preamble_len; /**< The pattern length in bits (1 - 16) */
	size_t   preamble_max;
	/**< A match is signal if the received bitstream matches this number of bits (1 - 16) */
	bool     preamble_unencoded;
	/**< Set to 1 to disable any encoding or scrambling during the preamble match */
	uint32_t sync;
	/**< The synchronization word can be up to 32 bits and should be given
	     in MSB first order */
	size_t   sync_len; /**< The SYNC word length in bits (1 - 32)*/
	size_t   sync_max;
	/**< A match is signal if the received bitstream matches this number of bits (1 - 32)*/
	bool     sync_unencoded;
	/**< Set to 1 to disable any encoding or scrambling during the SYNC match */
	uint8_t    en_inv;
	uint8_t    en_diff;
	uint8_t    en_scrambler;
	uint8_t    en_manch;
	uint8_t    en_nosync;
	struct crc crc;
};

struct tx_params {
	modulation_t mod;
	uint32_t     baudrate;
	uint32_t     bandwidth;
	rf_out_t     rf_out_mode;
	tx_shaping_t shaping;
	float        pout_dBm;
	/* Modulation dependent parameters  */
	union {
		struct psk_params psk;
		struct fsk_tx_params fsk;
		struct msk_tx_params msk;
		struct afsk_tx_params afsk;
	};
	/* Framing dependent parameters */
	framing_t    framing;
	union {
		struct raw_framing raw;
		struct hdlc_framing hdlc;
		struct pattern_framing raw_pattern;
	};
};

struct fsk_rx_params {
	float           mod_index;
};

struct fsk4_rx_params {
	uint8_t         manual_mode;
	leakage_t       leakage;
	float           max_deviation;
	float           min_deviation;
};

struct rx_params {
	modulation_t        mod;
	uint32_t            baudrate;
	uint32_t            bandwidth;
	freq_correction_t   freq_offset_corr;
	uint32_t            max_rf_offset;
	uint32_t            dr_offset;          //!< Maximum data rate offset
	bool                en_diversity;
	uint8_t             antesel;
	/* Modulation dependent parameters  */
	union {
		struct fsk_rx_params fsk;
		struct fsk4_rx_params fsk4;
	};
	/* Framing dependent parameters */
	framing_t            framing;
	union {
		struct pattern_framing raw_pattern;
	};
};

typedef int (*ax5043_spi_sel)(bool enable);
typedef int (*ax5043_spi)(uint8_t *rx, uint8_t *tx, uint32_t len);
typedef void (*ax5043_delay)(uint32_t period);
typedef uint32_t (*ax5043_millis)();
typedef void (*ax5043_tx_complete)();

/**
 * Callback that is called when a frame is received completely.
 * @param pdu pointer to the received frame
 * @param len the number of byte received
 */
typedef void (*ax5043_rx_complete)(const uint8_t *pdu, size_t len);

struct ax5043_conf_priv {
	bool             ready;
	bool             rf_init;
	uint8_t          f_xtaldiv;
	uint8_t          rfdiv;
	uint32_t         rf_freq;
	uint32_t         freqa;
	uint32_t         freqb;
	uint32_t         freqa_req;
	uint32_t         freqb_req;
	uint32_t         auto_rng_freqa;
	uint32_t         auto_rng_freqb;
	float            bb_freq;
	uint8_t          decimation;
	power_mode_t     pwr_mode;
	struct tx_params tx;
	bool             tx_valid;
	struct rx_params rx;
	bool             rx_valid;
};

/**
 * Configuration structure for the AX5043. All fields
 * except the \ref struct ax5043_conf.priv should be set by the user before the call
 * of \ref init() function
 */
struct ax5043_conf {
	struct xtal         xtal;             /**< The reference crystal setup */
	vco_mode_t          vco;              /**< Type of VCO used */
	ax5043_spi_sel      spi_select;       /**< Function pointer for SPI select */
	ax5043_spi          read;             /**< Function pointer for SPI read */
	ax5043_spi          write;            /**< Function pointer for SPI read */
	ax5043_delay        delay_us;         /**< Function pointer for
                                            with microsecond resolution delay */
	ax5043_millis       millis;
	/**< Function pointer for getting MCU time in ms */
	ax5043_tx_complete  tx_complete_clbk;
	/**< Pointer to a function that will be called after the completion of a frame
	transmission. Handy for RF switches or PAs. If set to NULL, the caller
	will skip this call.
	@note This function is called inside the ISR so should be ISR friendly,
	meaning that should not block and do as fast as possible its task */
	ax5043_rx_complete  rx_complete_clbk;
	/**< Pointer to a function that will be called after a frame is received.
	If set to NULL, the caller will skip this call.
	@note This function is called inside the ISR so should be ISR friendly,
	meaning that should not block and do as fast as possible its task */
	struct ax5043_conf_priv priv;
	/**< Private information for the driver. Do not edit this field manually */
};

bool
ax5043_ready(struct ax5043_conf *conf);

int
ax5043_reset(struct ax5043_conf *conf);

int
ax5043_prepare(struct ax5043_conf *conf, const struct xtal *xtal,
               vco_mode_t vco,
               int (*spi_sel)(bool enable),
               int (*spi_read)(uint8_t *rx, uint8_t *tx, uint32_t len),
               int (*spi_write)(uint8_t *rx, uint8_t *tx, uint32_t len),
               void (*delay_us)(uint32_t us),
               uint32_t (*millis)(),
               void (*tx_complete_callback)(),
               void (*rx_complete_callback)(const uint8_t *pdu, size_t len)
              );

int
ax5043_init(struct ax5043_conf *conf);

int
ax5043_set_power_mode(struct ax5043_conf *conf, power_mode_t mode);

int
ax5043_tx_conf_valid(struct ax5043_conf *conf, bool *valid);

int
ax5043_conf_tx(struct ax5043_conf *conf, const struct tx_params *params);

int
ax5043_set_tx_power(struct ax5043_conf *conf, float pout);

int
ax5043_set_tx_baud(struct ax5043_conf *conf, uint32_t baud);

int
ax5043_conf_rx(struct ax5043_conf *conf, const struct rx_params *params);

int
ax5043_tune(struct ax5043_conf *conf, freq_mode_t mode);

int
ax5043_config_freq(struct ax5043_conf *conf, freq_mode_t mode, uint32_t freq);

int
ax5043_aprs_framing_setup(struct ax5043_conf *conf);

int
ax5043_tx_frame(struct ax5043_conf *conf, const uint8_t *in, uint32_t len,
                uint32_t timeout_ms);

int
wait_xtal(struct ax5043_conf *conf, uint32_t timeout_ms);

int
ax5043_set_pinfuncantsel(struct ax5043_conf *conf, uint8_t weak_pullup,
                         uint8_t invert, pfantsel_t pfantsel);

int
ax5043_set_pinfuncpwramp(struct ax5043_conf *conf, uint8_t weak_pullup,
                         uint8_t invert, pfpwwramp_t pfpwramp);

int
ax5043_set_pwramp(struct ax5043_conf *conf, uint8_t val);

int
ax5043_conf_cw(struct ax5043_conf *conf);

int
ax5043_tx_cw(struct ax5043_conf *conf, uint32_t duration_ms);

int
ax5043_rssi(struct ax5043_conf *conf, float *rssi, float *bgnrssi);

int
ax5043_irq_callback();

#endif /* AX5043_H_ */
